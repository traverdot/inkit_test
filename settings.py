import os
import sys


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

sys.path.append(BASE_DIR)

DB_NAME = 'contacts.db'

PATH_TO_DB = os.path.join(BASE_DIR, DB_NAME)

