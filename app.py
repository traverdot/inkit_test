import falcon

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

from contacts.resources import ContactResource, ContactsResource
from settings import PATH_TO_DB
from contacts.middlewares import SQLAlchemySessionMiddleware, \
    CheckRequestJSONMiddleware


engine = create_engine('sqlite:///{}'.format(PATH_TO_DB))

session_factory = sessionmaker(bind=engine)
Session = scoped_session(session_factory)

api = application = falcon.API(
    middleware=[
        SQLAlchemySessionMiddleware(Session),
        CheckRequestJSONMiddleware()
    ])

contact = ContactResource()
contacts = ContactsResource()

api.add_route('/contacts/', contacts)
api.add_route('/contacts/{id}/', contact)
