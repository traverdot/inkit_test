def init_db_tables():
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker

    from contacts.models import Base, Contact, Address

    from settings import PATH_TO_DB

    engine = create_engine('sqlite:///{}'.format(PATH_TO_DB))

    Session = sessionmaker()
    Session.configure(bind=engine)

    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)

    contact_info = {
        'first_name': 'User',
        'last_name': 'User',
        'email': 'user@gmail.com',
        'phone_number': 123456789,
        'company': 'Company',
        'notes': 'Some notes...',
    }

    contact_address = {
        'zip_code': 12345,
        'street_address': 'Shevchenko str. 78',
        'unit_number_addr_line_2': 'addr line 2',
        'country': 'UA',
        'state': 'ZP',
        'city': 'Zaporijja'
        }


    contact = Contact(**contact_info)
    address = Address(**contact_address)
    contact.address = address

    session = Session()
    session.add(contact)
    session.commit()

    session.close()


if __name__ == '__main__':
    init_db_tables()
