import json
import falcon


class SQLAlchemySessionMiddleware(object):
    """
    Create a SQLAlchemy session for every request and close it when it ends.
    """

    def __init__(self, session):
        self.Session = session

    def process_resource(self, req, resp, resource, params):
        resource.session = self.Session()

    def process_response(self, req, resp, resource, req_succeeded):
        if hasattr(resource, 'session'):
            if not req_succeeded:
                resource.session.rollback()
            self.Session.remove()


class CheckRequestJSONMiddleware(object):
    """ Check for right JSON format in request """

    def process_request(self, req, resp):
        if req.method in ['POST', 'PUT', 'PATCH']:
            if not req.content_type or 'application/json' not in req.content_type:
                raise falcon.HTTPError(falcon.HTTP_400,
                                       'Invalid JSON in request')
            try:
                body = json.dumps(req.media)
            except Exception as err:
                raise falcon.HTTPError(falcon.HTTP_400,
                                       'Invalid JSON in request')
