from marshmallow import fields, Schema


class AddressSerializer(Schema):
    """Address schema"""
    id = fields.Integer()
    street_address = fields.Str(required=True)
    unit_number_addr_line_2 = fields.Str()
    city = fields.Str(required=True)
    state = fields.Str(required=True)
    zip_code = fields.Integer(required=True)
    country = fields.Str()


class ContactSerializer(Schema):
    """Contact schema"""
    id = fields.Integer()
    first_name = fields.Str(required=True)
    last_name = fields.Str(required=True)
    email = fields.Email(required=True)
    phone_number = fields.Integer(error_messages={'invalid': 'Not a valid phone format number.'})
    company = fields.Str()
    address = fields.Nested(AddressSerializer(), required=True)
    notes = fields.Str()
