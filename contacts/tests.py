import json
import pytest

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from falcon import testing

from app import api
from settings import PATH_TO_DB
from contacts.models import Base, Contact, Address
from contacts.serializers import ContactSerializer


def setup_module(module):
    engine = create_engine('sqlite:///{}'.format(PATH_TO_DB))
    Base.metadata.bind = engine
    Base.metadata.create_all(engine)


@pytest.fixture()
def contact_data():
    return {
        'first_name': 'User',
        'last_name': 'User',
        'email': 'user@gmail.com',
        'phone_number': 123456789,
        'company': 'Company',
        'notes': 'Some notes...',
        'address': {
            'zip_code': 12345, 
            'street_address': 'Shevchenko str. 78',
            'unit_number_addr_line_2': 'addr line 2',
            'country': 'UA',
            'state': 'ZP',
            'city': 'Zaporijja'
        }
    }


@pytest.fixture(scope='function')
def db_session():
    engine = create_engine('sqlite:///{}'.format(PATH_TO_DB))
    DBSession = sessionmaker(bind=engine)
    return DBSession()


@pytest.fixture(scope='module')
def client():
    return testing.TestClient(api)


def cleanup_db(db_session):
    db_session.query(Contact).delete
    db_session.query(Address).delete
    db_session.commit()


class TestAPI(object):
    """ API tests """

    headers = {"Content-Type": "application/json"}

    @staticmethod
    def create_contact_with_alchemy(db_session):
        contact_dict = contact_data()

        contact_address = Address(**contact_dict.pop('address'))
        contact = Contact(**contact_dict)
        contact.address = contact_address

        db_session.add(contact)
        db_session.commit()

        return contact

    @staticmethod
    def get_contact_with_alchemy(db_session, contact_id):
        # because we are working with the same session
        db_session.commit()

        try:
            contact = db_session.query(Contact).filter_by(id=contact_id).one()
        except Exception:
            return None

        return contact

    @staticmethod
    def del_contact_with_alchemy(db_session, db_contact):
        if db_contact:
            db_session.query(Address).filter_by(id=db_contact.address_id).delete()
            db_session.delete(db_contact)
            db_session.commit()

    def test_not_json_acceptance(self, client):
        std = {'title': 'Invalid JSON in request'}

        result = client.simulate_post(
            '/contacts/', body='not json string', headers=self.headers).json

        assert std == result

    def test_get_contact_collection(self, db_session, client):
        # lets create two contacts with alchemy
        db_contact_obj1 = self.create_contact_with_alchemy(db_session)
        db_contact_obj2 = self.create_contact_with_alchemy(db_session)

        # try to receive a list of contacts with API
        result = client.simulate_get('/contacts/').json

        # delete test contacts
        self.del_contact_with_alchemy(db_session, db_contact_obj1)
        self.del_contact_with_alchemy(db_session, db_contact_obj2)

        assert len(result) > 1

    def test_get_contact(self, db_session, client):
        contact_schema = ContactSerializer()

        # create test contact with alchemy
        db_contact_obj = self.create_contact_with_alchemy(db_session)
        db_contact = contact_schema.dump(db_contact_obj).data

        result = client.simulate_get(
            '/contacts/{}'.format(db_contact['id'])).json

        # delete test contact
        self.del_contact_with_alchemy(db_session, db_contact_obj)

        assert result == db_contact

    def test_create_contact(self, client):
        contact_dict = contact_data()
        contact_json = json.dumps(contact_dict)

        result = client.simulate_post(
            '/contacts/', body=contact_json, headers=self.headers).json

        # just delete ids of contact and address to compare
        # with standard contact dict
        contact_id = result.pop('id')
        contact_address_id = result['address'].pop('id')

        # delete test contact
        client.simulate_delete('/contacts/{}'.format(contact_id))

        assert result == contact_dict

    def test_create_contact_without_required_fields(self, client):
        contact_dict = contact_data()

        # delete first_name and zip_code from request
        del contact_dict['first_name']
        del contact_dict['address']['zip_code']

        std = {
            'first_name': ['Missing data for required field.'],
            'address': {'zip_code': ['Missing data for required field.']}
        }

        result = client.simulate_post(
            '/contacts/',
            body=json.dumps(contact_dict),
            headers=self.headers
        ).json

        assert result == std

    def test_put_contact_update(self, db_session, client):
        contact_schema = ContactSerializer()

        contact_dict = contact_data()
        db_contact_obj = self.create_contact_with_alchemy(db_session)
        db_contact = contact_schema.dump(db_contact_obj).data

        # change last_name and state
        contact_dict['last_name'] = 'New Lastname'
        contact_dict['address']['state'] = 'New State'

        result = client.simulate_put(
            '/contacts/{}'.format(db_contact['id']),
            body=json.dumps(contact_dict),
            headers=self.headers
        )

        updated_db_contact = contact_schema.dump(
            self.get_contact_with_alchemy(db_session, db_contact['id'])
        ).data

        # delete test contact
        self.del_contact_with_alchemy(db_session, db_contact_obj)

        assert db_contact['last_name'] != updated_db_contact['last_name']
        assert db_contact['address']['state'] != updated_db_contact['address']['state']

    def test_partial_contact_update(self, db_session, client):
        contact_schema = ContactSerializer()

        db_contact_obj = self.create_contact_with_alchemy(db_session)
        db_contact = contact_schema.dump(db_contact_obj).data

        # update only first_name and last_name
        contact_dict = {
            'first_name': 'New Firstname',
            'last_name': 'New Lastname'
        }

        result = client.simulate_patch(
            '/contacts/{}'.format(db_contact['id']),
            body=json.dumps(contact_dict),
            headers=self.headers
        )

        updated_db_contact = contact_schema.dump(
            self.get_contact_with_alchemy(db_session, db_contact['id'])
        ).data

        # delete test contact
        self.del_contact_with_alchemy(db_session, db_contact_obj)

        assert db_contact['first_name'] != updated_db_contact['first_name']
        assert db_contact['last_name'] != updated_db_contact['last_name']

    def test_delete_contact(self, db_session, client):
        contact_schema = ContactSerializer()

        db_contact_obj = self.create_contact_with_alchemy(db_session)
        db_contact = contact_schema.dump(db_contact_obj).data

        result = client.simulate_delete(
                '/contacts/{}'.format(db_contact['id'])
        )

        deleted_contact = self.get_contact_with_alchemy(
            db_session,
            db_contact['id']
        )

        assert deleted_contact == None
