from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()


class Contact(Base):
    __tablename__ = 'user_contacts'

    id = Column(Integer, primary_key=True)
    first_name = Column(String(25), nullable=False)
    last_name = Column(String(25), nullable=False)
    email = Column(String(50), nullable=False)
    phone_number = Column(Integer)
    company = Column(String(25))
    address_id = Column(Integer, ForeignKey('contact_address.id'), nullable=False)
    address = relationship('Address',
                           backref=backref('contact', uselist=False))
    notes = Column(String(200))

    def __repr__(self):
        return 'ID:{}, {} {}'.format(self.id, self.first_name, self.last_name)


class Address(Base):
    __tablename__ = 'contact_address'

    id = Column(Integer, primary_key=True)
    street_address = Column(String(50), nullable=False)
    unit_number_addr_line_2 = Column(String(50))
    city = Column(String(25), nullable=False)
    state = Column(String(25), nullable=False)
    zip_code = Column(Integer, nullable=False)
    country = Column(String(25), default='US')

    def __repr__(self):
        return 'ID:{}, {}, {}, {}'.format(self.id, self.country,
                                          self.city, self.street_address)


