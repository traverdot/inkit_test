import json

import falcon
from marshmallow import ValidationError

from contacts.models import Contact, Address
from contacts.serializers import ContactSerializer, AddressSerializer


class ContactsResource(object):
    """ Resource for GET collection of contacts and POST new contact """

    def on_get(self, req, resp):
        contact_schema = ContactSerializer(many=True)
        contacts = self.session.query(Contact).all()
        resp.body = contact_schema.dumps(contacts).data

    def on_post(self, req, resp):
        contact_schema = ContactSerializer()

        try:
            new_contact = contact_schema.load(req.media)

            # here we check errors manually because of schema.load.
            # It returns namedtuple('UnmarshalResult', ['data', 'errors'])
            if new_contact.errors:
                raise ValidationError(new_contact.errors)

        except ValidationError as err:
            resp.body = json.dumps(err.messages)
            resp.status = falcon.HTTP_400
            return

        # create contact and its address (two linked models)
        new_contact_address = Address(**new_contact.data.pop('address'))
        new_contact = Contact(**new_contact.data)
        new_contact.address = new_contact_address

        try:
            self.session.add(new_contact)
            self.session.commit()

        except Exception as err:
            resp.body = json.dumps(str(err))
            resp.status = falcon.HTTP_400
            return

        resp.body = contact_schema.dumps(new_contact).data
        resp.status = falcon.HTTP_201


class ContactResource(object):
    """ Resource for GET, PUT, PATCH, DELETE for defined contact """

    def __init__(self, *args, **kwargs):
        super(ContactResource, self).__init__(*args, **kwargs)
        self.schema = ContactSerializer()

    def if_contact_exists(req, resp, resource, params):
        """ Decorator for checking existence of defined contact """
        try:
            contact = resource.session.query(Contact).filter_by(
                id=params['id']
            ).one()

            # store Contact instance for continued using. We need ids.
            req.params['contact'] = contact
        except Exception as err:
            raise falcon.HTTPBadRequest('Bad request',
                                        'contact doesn\'t exist')

    def _update_contact_info(self, req, contact_id, contact_new_info):
        """ update defined contact info """
        try:
            # just del id from contact_new_info dict
            id = contact_new_info.data.pop('id', None)
            contact_address = contact_new_info.data.pop('address', None)

            # update contact
            self.session.query(Contact).filter_by(
                id=contact_id).update(contact_new_info.data)

            # update contact's address
            if contact_address:
                # just del address id from address dict if it is.
                contact_address_id = contact_address.pop('id', None)

                self.session.query(Address).filter_by(
                    id=req.params['contact'].address_id).update(contact_address)

            self.session.commit()

        except Exception as err:
            raise falcon.HTTPBadRequest('Bad request',
                                        'Contact is not updated.')

    @falcon.before(if_contact_exists)
    def on_get(self, req, resp, id):
        resp.body = self.schema.dumps(req.params['contact']).data

    @falcon.before(if_contact_exists)
    def on_put(self, req, resp, id):
        try:
            contact_new_info = self.schema.load(req.media)

            if contact_new_info.errors:
                raise ValidationError(contact_new_info.errors)

        except ValidationError as err:
            resp.body = json.dumps(err.messages)
            resp.status = falcon.HTTP_400
            return

        self._update_contact_info(req, id, contact_new_info)
        resp.status = falcon.HTTP_200

    @falcon.before(if_contact_exists)
    def on_patch(self, req, resp, id):
        try:
            contact_new_info = self.schema.load(req.media, partial=True)

            if contact_new_info.errors:
                raise ValidationError(contact_new_info.errors)

        except ValidationError as err:
            resp.body = json.dumps(err.messages)
            resp.status = falcon.HTTP_400
            return

        self._update_contact_info(req, id, contact_new_info)
        resp.status = falcon.HTTP_200

    @falcon.before(if_contact_exists)
    def on_delete(self, req, resp, id):
        # delete Contact instance and its related Address instance
        try:
            self.session.query(Contact).filter_by(id=id).delete()
            self.session.query(Address).filter_by(
                id=req.params['contact'].address_id
            ).delete()

            self.session.commit()

        except Exception as err:
            raise falcon.HTTPBadRequest(
                'Bad request',
                'Contact is not deleted.'
            )
