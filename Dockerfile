FROM python:2.7-slim
WORKDIR /app
ADD . /app
RUN pip install -r requirements.txt
EXPOSE 8000

CMD ["python", "init_db.py"]
CMD ["gunicorn", "-b", "0.0.0.0:8000",  "--reload", "app"]
