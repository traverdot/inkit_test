**Installation**

- clone project: 
            `git clone https://traverdot@bitbucket.org/traverdot/inkit_test.git`

- create environment for project (it may be for python3): 
            `virtualenv venv`
            or
            `virtualenv -p python3 venv`

- activate environment: 
            `. venv/bin/activate`

- move to project's root dir

- install requirements: 
            `pip install -r requirements.txt`

- create SQLite DB by using init_db.py script. DB will be located in project's root dir.
            `python init_db.py`

- to run tests there is contacts/tests.py script: 
            `pytest contacts/tests.py -v`

- to run server: 
            `gunicorn --reload app`

- to containerize project with Docker:
    - move to project's root dir and build image: 
            `docker build -t imagename .`

    - run docker container: 
            `docker run -p 8000:8000 -t imagename`
            

**Available endpoints:**

- `/contacts/` - to get a collection of contacts with method GET. With POST method it is used to create new contact.
- `/contacts/{id}/` - to GET/PUT/PATCH/DELETE defined contact.